const Router = require('express').Router();
const Controller = require('../controllers/clips-controller');

Router
  .get('/clips/:id', Controller.show)
  .post('/clips/create', Controller.create);

module.exports = Router;
