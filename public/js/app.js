$(document).ready(function () {
  var $form = $('#clip-form'),
    $submitBtn = $form.find('button[type=submit]'),
    $player = $('#player');

  $form.on('submit', function (e) {
    e.preventDefault();

    $submitBtn.attr('disabled', true);
    $submitBtn.text('Processing...');

    $.ajax({
      type: "POST",
      url: '/api/clips/create',
      data: $(this).serialize(),
      success: function (data) {
        $player.attr('src', location.origin + '/api/clips/' + data.clipId);
        $player.show();
      },
      error: function (err) {
        alert('Unexpected error');
        console.log(err)
      }
    }).always(function () {
      $submitBtn.removeAttr('disabled');
      $submitBtn.text('Generate clip');

      $form.reset()
    });
  });
});
