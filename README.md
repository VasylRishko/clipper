
## Tech stack

- node.js - `8.9.3`
- yarn - `1.3.2`
- ffmpeg - `3.4`

## Run
Make sure you have installed `ffmpeg`. Or you can [download](https://www.ffmpeg.org/download.html) binary file and export FFMPEG_PATH env variable as a full path of executable `ffmpeg`.

1. `npm i yarn -g`
1. `yarn install`
1. `yarn start`
