global.Promise = require('bluebird'); // using bluebird

const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const clipRoutes = require('./routes/clip-routes');
const staticServe = express.static('public');

const app = express();
const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || 'development';

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));

// Routes
app.use('/api', clipRoutes);

// Public assets
app.use(staticServe);

// Start server
app.listen(port, () => {
  console.log('Server started on port: %s, environment: %s ...\n', port, env)
});

module.exports = app;
