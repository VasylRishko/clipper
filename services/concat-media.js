const joinPath = require('path').join;
const ffmpeg = require('fluent-ffmpeg');

const tmpDirPath = joinPath(__dirname, '../tmp');

/**
 * Concat media files in single file
 * @param {String} type     - Type of file (audio/video)
 * @param {Number} fileId   - id of created audio or video file
 * @param {Object} options  - source id, start clip, stop clip
 * @return {Promise}
 */
function concat (type, fileId, options = {}) {
  return new Promise((resolve, reject) => {
    options = Object.assign({}, options[type]);

    const sourceId = options.source;
    const start = parseInt(options.start_clip);
    const stop = parseInt(options.stop_clip);

    const newFilePath = joinPath(tmpDirPath, `${type}-${fileId}.mp4`);
    const ffmpegCommand = ffmpeg();

    for (let i = start; i <= stop; i++) {
     ffmpegCommand.input(fullFilePath(type, sourceId, i))
    }

    ffmpegCommand.on('error', reject);
    ffmpegCommand.on('end', () => resolve(fileId));

    ffmpegCommand.mergeToFile(newFilePath, tmpDirPath);
  })
}

// Helpers
function fullFilePath (type, id, index) {
  return joinPath(
    __dirname,
    '../static',
    `${type}${id}`,
    fileName(...arguments)
  )
}

function fileName (type, id, index) {
  index = String(index).padStart(3, 0);

  const prefix = isAudio(type) ? 'mic' : 'camera';

  return `${prefix}${id}_clip_${index}.mp4`
}

function isAudio (type) {
  return type === 'audio'
}


module.exports = concat;
