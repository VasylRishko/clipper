const { unlink } = require('fs');
const joinPath = require('path').join;
const ffmpeg = require('fluent-ffmpeg');
const tmpDirPath = joinPath(__dirname, '../tmp');
const clipsDirPath = joinPath(__dirname, '../static/clips');

/**
 * Merge video and audio into single file
 * @param {Number} id     - id of created audio and video file in tmp directory
 * @param {Object} options
 * @return {Promise}
 */
function merge (id, options = {}) {
  return new Promise((resolve, reject) => {
    const clipPath = joinPath(clipsDirPath, `clip-${id}.mp4`);

    ffmpeg()
      .input(filePath('video', id))
      .inputOptions('-itsoffset', getOffset(options.video.offset))
      .input(filePath('audio', id))
      .inputOptions('-itsoffset', getOffset(options.audio.offset))
      .output(clipPath)
      .on('error', reject)
      .on('end', () => resolve(id))
      .run();
  })
}

// Helpers
function filePath (type, id) {
  return joinPath(tmpDirPath, `${type}-${id}.mp4`);
}

function removeTmpFiles (id) {
  const logError = (err) => err && console.log('Error when deleting tmp file:', err);

  unlink(filePath('audio', id), logError);
  unlink(filePath('video', id), logError)
}

function getOffset (offset) {
  offset = parseInt(offset) || 0;
  return offset / 1000 + ''
}

module.exports = merge;
module.exports.removeTmpFiles = removeTmpFiles;
