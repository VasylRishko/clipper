const uniqid = require('uniqid');
const { exists } = require('fs');
const joinPath = require('path').join;
const ConcatMedia = require('../services/concat-media');
const MergeVideoAudio = require('../services/merge-video-audio');
const removeTmpFiles = MergeVideoAudio.removeTmpFiles;

exports.show = function (req, res ,next) {
  const id = req.params.id;
  const filePath = joinPath(__dirname, '../static/clips', `clip-${id}.mp4`);

  exists(filePath, (exs) => {
    if (!exs) {
      return res.status(404).json({ message: 'Clip not found', id: id })
    }

    res.sendFile(filePath)
  });
};

exports.create = function (req, res ,next) {
  // TODO: validate req.body, all keys should be present, stop_clip > start_clip

  const newClipId = uniqid();

  Promise.all([
    ConcatMedia('audio', newClipId, req.body),
    ConcatMedia('video', newClipId, req.body)
  ])
  .then(() => MergeVideoAudio(newClipId, req.body))
  .then((id) => {
    res.json({ status: 'ok', clipId: id })
  })
  .catch((err) => {
    res.status(500).json({ status: 'error' })
  })
  .then(() => removeTmpFiles(newClipId))
};
